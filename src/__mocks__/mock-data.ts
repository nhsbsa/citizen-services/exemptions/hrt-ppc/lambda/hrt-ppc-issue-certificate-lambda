export { default as mockSuccessResponse } from "./success-response.json";
export { default as mockCitizen } from "./citizen-api-request.json";
export { default as mockCitizenResponse } from "./citizen-api-response.json";
export { default as mockCertificate } from "./certificate-api-request.json";
export { default as mockCertificateResponse } from "./certificate-api-response.json";
export { default as mockUserPreferenceResponse } from "./user-preference-api-response.json";
export { default as mockHeaders } from "./headers.json";
