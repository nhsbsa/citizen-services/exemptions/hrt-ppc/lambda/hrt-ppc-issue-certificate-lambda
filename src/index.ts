import { APIGatewayEvent } from "aws-lambda";
import {
  loggerWithContext,
  getHeaderValue,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import {
  successResponse,
  errorResponse,
  BadRequest,
  transformHeadersAndReturnMandatory,
  FieldError,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { issueCertificate } from "./hrt-ppc-issue-certificate";

const logRequest = (headers, requestId) => {
  const correlationId = getHeaderValue(headers, "correlation-id");
  const channel = getHeaderValue(headers, "channel");
  const userId = getHeaderValue(headers, "user-id");

  loggerWithContext().info(
    `Request received [requestId: ${requestId}][correlationId: ${correlationId}][userId: ${userId}][channel: ${channel}]`,
  );
};

export async function handler(event: APIGatewayEvent) {
  const requestTime = new Date();

  try {
    loggerWithContext().info("hrt-ppc-issue-certificate-lambda");
    const {
      headers,
      body,
      requestContext: { requestId },
    } = event;

    setCorrelationId(getHeaderValue(headers, "correlation-id"));

    const mandatoryHeaders = transformHeadersAndReturnMandatory(headers);

    logRequest(mandatoryHeaders, requestId);

    const payloadBody = JSON.parse(body || "{}");

    const { citizen, certificate, userPreference } = payloadBody;
    const fieldErrors: FieldError[] = [];
    if (!citizen) {
      const fieldError = {
        field: "citizen",
        message: "must not be null or empty",
      };
      fieldErrors.push(fieldError);
    }

    if (!certificate) {
      const fieldError = {
        field: "certificate",
        message: "must not be null or empty",
      };
      fieldErrors.push(fieldError);
    }

    if (userPreference == "EMAIL" && !citizen?.emails?.[0]?.emailAddress) {
      const fieldError = {
        field: "userPreference",
        message: "User preference set to EMAIL but no email address provided",
      };
      fieldErrors.push(fieldError);
    }

    if (fieldErrors.length) {
      const message = "There were validation issues with the request";
      throw new BadRequest(message, requestTime, fieldErrors);
    }
    loggerWithContext().info("Request body validated");

    const result = await issueCertificate({
      headers: mandatoryHeaders,
      citizen,
      certificate,
      userPreference,
    });
    return successResponse(200, result);
  } catch (err) {
    loggerWithContext().error({ message: err });
    return errorResponse(err);
  }
}
