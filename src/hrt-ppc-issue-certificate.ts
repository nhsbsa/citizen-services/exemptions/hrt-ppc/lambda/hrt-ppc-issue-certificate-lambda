import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import {
  CitizenApi,
  CertificateApi,
  UserPreferenceApi,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

async function persistCitizen(citizen, headers) {
  return new CitizenApi().makeRequest({
    method: "POST",
    url: "/v1/citizens",
    data: citizen,
    headers,
    responseType: "json",
  });
}

async function persistCertificate(certificate, citizenId, headers) {
  return new CertificateApi().makeRequest({
    method: "POST",
    url: "/v1/certificates",
    params: { citizenId },
    data: certificate,
    headers,
    responseType: "json",
  });
}

async function persistUserPreference(userPreference, citizenId, headers) {
  return new UserPreferenceApi().makeRequest({
    method: "POST",
    url: "/v1/user-preference",
    params: { citizenId },
    data: userPreference,
    headers,
    responseType: "json",
  });
}

function hasEmail(citizen) {
  return (
    citizen.emails &&
    Array.isArray(citizen.emails) &&
    citizen.emails.length > 0 &&
    citizen.emails[0].emailAddress
  );
}

function getEvent(headers, citizen) {
  return headers.channel === "ONLINE" && !hasEmail(citizen)
    ? "REMINDER"
    : "ANY";
}

function getPreference(userPreference, citizen) {
  return userPreference ?? (hasEmail(citizen) ? "EMAIL" : "POSTAL");
}

export async function issueCertificate({
  headers,
  citizen,
  certificate,
  userPreference,
}) {
  loggerWithContext().info("Entered issue certificate service");

  const citizenResponse = await persistCitizen(citizen, headers);
  loggerWithContext().info(
    `Citizen record saved successfully, citizenId:[${citizenResponse.id}]`,
  );

  const userPreferenceRequest = {
    certificateType: certificate.type,
    event: getEvent(headers, citizen),
    preference: getPreference(userPreference, citizen),
  };

  const [certificateResponse, userPreferenceResponse] = await Promise.all([
    persistCertificate(certificate, citizenResponse.id, headers).then(
      (response) => {
        loggerWithContext().info(
          `Certificate record saved successfully, certificateId:[${response.id}, reference: [${response.reference}]]`,
        );
        return response;
      },
    ),
    persistUserPreference(
      userPreferenceRequest,
      citizenResponse.id,
      headers,
    ).then((response) => {
      loggerWithContext().info(
        `User Preference record saved successfully, userPreferenceId:[${response.id}]`,
      );
      return response;
    }),
  ]);

  return {
    citizen: citizenResponse,
    certificate: certificateResponse,
    userPreference: userPreferenceResponse,
  };
}
