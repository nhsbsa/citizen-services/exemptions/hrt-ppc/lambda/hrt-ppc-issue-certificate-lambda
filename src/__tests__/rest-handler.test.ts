import { handler } from "../index";

import { issueCertificate } from "../hrt-ppc-issue-certificate";
import { mockCitizen, mockSuccessResponse } from "../__mocks__/mock-data";

import mockEvent from "../../events/event.json";

jest.mock("../hrt-ppc-issue-certificate");

let event;
const mockIssueCertificate = issueCertificate as jest.MockedFunction<
  typeof issueCertificate
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEvent));
  mockIssueCertificate.mockClear();
  mockIssueCertificate.mockResolvedValue(mockSuccessResponse);
});

describe("handler", () => {
  it("should respond with a bad request error if req body is an empty object", async () => {
    event.body = JSON.stringify({});
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"citizen","message":"must not be null or empty"},{"field":"certificate","message":"must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with a bad request error if request citizen field is empty", async () => {
    event.body = JSON.stringify({
      citizen: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"certificate","message":"must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with a bad request error if request certificate field is empty", async () => {
    event.body = JSON.stringify({
      certificate: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"citizen","message":"must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with a bad request error if userPreference is EMAIL but no emails", async () => {
    event.body = JSON.stringify({
      userPreference: "EMAIL",
      citizen: {},
      certificate: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"userPreference","message":"User preference set to EMAIL but no email address provided"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with a bad request error if userPreference is EMAIL but emails an empty array", async () => {
    event.body = JSON.stringify({
      userPreference: "EMAIL",
      citizen: { emails: [] },
      certificate: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"userPreference","message":"User preference set to EMAIL but no email address provided"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with a bad request error if userPreference is EMAIL but emailAddress is empty", async () => {
    event.body = JSON.stringify({
      userPreference: "EMAIL",
      citizen: { ...mockCitizen, emails: { emailAddress: undefined } },
      certificate: {},
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"userPreference","message":"User preference set to EMAIL but no email address provided"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with an internal server error", async () => {
    mockIssueCertificate.mockRejectedValueOnce(Error("Internal Server Error"));

    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with the 200 success response when certificate issued successfully", async () => {
    const result = await handler(event);
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(mockSuccessResponse),
    });
  });

  it("should ignore the additional headers and pass mandatory headers to handler", async () => {
    event.headers = {
      channel: "ONLINE",
      "user-id": "ABCD",
      "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
      additionalHeader: "ignored",
    };
    await handler(event);
    expect(mockIssueCertificate).toHaveBeenCalledTimes(1);
    expect(mockIssueCertificate.mock.calls[0][0].headers)
      .toMatchInlineSnapshot(`
      {
        "channel": "ONLINE",
        "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
        "user-id": "ABCD",
      }
    `);
  });
});
