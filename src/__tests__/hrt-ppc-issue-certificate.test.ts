import { issueCertificate } from "../hrt-ppc-issue-certificate";
import {
  CertificateApi,
  CitizenApi,
  InternalError,
  UserPreferenceApi,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  mockCertificate,
  mockCertificateResponse,
  mockCitizen,
  mockCitizenResponse,
  mockHeaders,
  mockSuccessResponse,
  mockUserPreferenceResponse,
} from "../__mocks__/mock-data";

let citizenApiSpyInstance: jest.SpyInstance;
let certificateApiSpyInstance: jest.SpyInstance;
let userPreferenceApiSpyInstance: jest.SpyInstance;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  citizenApiSpyInstance = jest
    .spyOn(CitizenApi.prototype, "makeRequest")
    .mockResolvedValue(mockCitizenResponse);
  certificateApiSpyInstance = jest
    .spyOn(CertificateApi.prototype, "makeRequest")
    .mockResolvedValue(mockCertificateResponse);
  userPreferenceApiSpyInstance = jest
    .spyOn(UserPreferenceApi.prototype, "makeRequest")
    .mockResolvedValue(mockUserPreferenceResponse);
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
});

describe("issue certificate", () => {
  it("should respond with the success response", async () => {
    const result = await issueCertificate({
      headers: mockHeaders,
      citizen: mockCitizen,
      certificate: mockCertificate,
      userPreference: "POSTAL",
    });
    expect(result).toEqual(mockSuccessResponse);
  });

  it("citizen api client should be called with the correct parameters", async () => {
    await issueCertificate({
      headers: mockHeaders,
      citizen: mockCitizen,
      certificate: mockCertificate,
      userPreference: undefined,
    });
    expect(citizenApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(citizenApiSpyInstance.mock.calls[0]).toMatchInlineSnapshot(`
      [
        {
          "data": {
            "addresses": [
              {
                "addressLine1": "Stella House",
                "addressLine2": "Goldcrest Way",
                "country": "England",
                "postcode": "NE15 8NY",
                "townOrCity": "Newcastle upon Tyne",
              },
            ],
            "dateOfBirth": "2022-11-18",
            "emails": [
              {
                "emailAddress": "test@example.com",
              },
            ],
            "firstName": "John",
            "lastName": "Smith",
            "nhsNumber": "5165713040",
            "telephones": [
              {
                "phoneNumber": "+447123456789",
              },
            ],
          },
          "headers": {
            "channel": "ONLINE",
            "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
            "user-id": "ABCD",
          },
          "method": "POST",
          "responseType": "json",
          "url": "/v1/citizens",
        },
      ]
    `);
  });

  it("certificate api client should be called with the correct parameters", async () => {
    await issueCertificate({
      headers: mockHeaders,
      citizen: mockCitizen,
      certificate: mockCertificate,
      userPreference: undefined,
    });
    expect(certificateApiSpyInstance).toHaveBeenCalledTimes(1);
    expect(certificateApiSpyInstance.mock.calls[0]).toMatchInlineSnapshot(`
      [
        {
          "data": {
            "applicationDate": "2022-09-14",
            "cost": 1870,
            "duration": "P12M",
            "endDate": "2023-09-13",
            "pharmacyId": "ABCD",
            "startDate": "2022-09-14",
            "type": "HRT_PPC",
          },
          "headers": {
            "channel": "ONLINE",
            "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
            "user-id": "ABCD",
          },
          "method": "POST",
          "params": {
            "citizenId": "0a004b01-857c-1e59-8185-7da2d678004e",
          },
          "responseType": "json",
          "url": "/v1/certificates",
        },
      ]
    `);
  });

  describe("user preference api client should be called with", () => {
    it.each([
      {
        channel: "ONLINE",
        emails: undefined,
        userPreference: undefined,
        expectedEvent: "REMINDER",
        expectedPreference: "POSTAL",
      },
      {
        channel: "ONLINE",
        emails: [],
        userPreference: undefined,
        expectedEvent: "REMINDER",
        expectedPreference: "POSTAL",
      },
      {
        channel: "ONLINE",
        emails: [{ emailAddress: "" }],
        userPreference: undefined,
        expectedEvent: "REMINDER",
        expectedPreference: "POSTAL",
      },
      {
        channel: "ONLINE",
        emails: [{ emailAddress: "test@example.com" }],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "EMAIL",
      },
      {
        channel: "PHARMACY",
        emails: undefined,
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "PHARMACY",
        emails: [],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "PHARMACY",
        emails: [{ emailAddress: "" }],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "PHARMACY",
        emails: [{ emailAddress: "test@example.com" }],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "EMAIL",
      },
      {
        channel: "STAFF",
        emails: undefined,
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [{ emailAddress: "" }],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [{ emailAddress: "test@example.com" }],
        userPreference: undefined,
        expectedEvent: "ANY",
        expectedPreference: "EMAIL",
      },
      {
        channel: "STAFF",
        emails: [{ emailAddress: "test@example.com" }],
        userPreference: "EMAIL",
        expectedEvent: "ANY",
        expectedPreference: "EMAIL",
      },
      {
        channel: "STAFF",
        emails: undefined,
        userPreference: "POSTAL",
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [{ emailAddress: "test@example.com" }],
        userPreference: "POSTAL",
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [],
        userPreference: "POSTAL",
        expectedEvent: "ANY",
        expectedPreference: "POSTAL",
      },
      {
        channel: "STAFF",
        emails: [{}],
        userPreference: "EMAIL",
        expectedEvent: "ANY",
        expectedPreference: "EMAIL",
      },
    ])(
      "event=$expectedEvent, preference=$expectedPreference when channel=$channel and emails=$emails",
      async ({
        channel,
        emails,
        userPreference,
        expectedEvent,
        expectedPreference,
      }) => {
        const headers = { ...mockHeaders, channel };
        const citizen = { ...mockCitizen, emails };
        await issueCertificate({
          headers,
          citizen,
          certificate: mockCertificate,
          userPreference: userPreference,
        });
        expect(userPreferenceApiSpyInstance).toHaveBeenCalledTimes(1);
        expect(userPreferenceApiSpyInstance.mock.calls[0])
          .toMatchInlineSnapshot(`
      [
        {
          "data": {
            "certificateType": "HRT_PPC",
            "event": "${expectedEvent}",
            "preference": "${expectedPreference}",
          },
          "headers": {
            "channel": "${channel}",
            "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
            "user-id": "ABCD",
          },
          "method": "POST",
          "params": {
            "citizenId": "0a004b01-857c-1e59-8185-7da2d678004e",
          },
          "responseType": "json",
          "url": "/v1/user-preference",
        },
      ]
    `);
      },
    );
  });

  it("should throw an internal server error when citizen api has an error response", async () => {
    citizenApiSpyInstance = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockRejectedValueOnce(new Error("citizen api error"));

    await expect(async () =>
      issueCertificate({
        headers: mockHeaders,
        citizen: mockCitizen,
        certificate: mockCertificate,
        userPreference: "EMAIL",
      }),
    ).rejects.toThrow(new InternalError("citizen api error", new Date()));
  });

  it("should throw an internal server error when certificate api has an error response", async () => {
    certificateApiSpyInstance = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockRejectedValueOnce(new Error("certificate api error"));

    await expect(async () =>
      issueCertificate({
        headers: mockHeaders,
        citizen: mockCitizen,
        certificate: mockCertificate,
        userPreference: "EMAIL",
      }),
    ).rejects.toThrow(new InternalError("certificate api error", new Date()));
  });

  it("should throw an internal server error when user preference api has an error response", async () => {
    userPreferenceApiSpyInstance = jest
      .spyOn(UserPreferenceApi.prototype, "makeRequest")
      .mockRejectedValueOnce(new Error("user preference api error"));

    await expect(async () =>
      issueCertificate({
        headers: mockHeaders,
        citizen: mockCitizen,
        certificate: mockCertificate,
        userPreference: "EMAIL",
      }),
    ).rejects.toThrow(
      new InternalError("user preference api error", new Date()),
    );
  });
});
